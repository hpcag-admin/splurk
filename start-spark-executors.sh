#!/bin/bash 

#SLURM configs
export SPARK_LOG_DIR=${HOME}/.spark/${SLURM_JOB_ID}
mkdir -p $SPARK_LOG_DIR
mkdir -p $SPARK_LOG_DIR/conf
export SPARK_CONF_DIR=$SPARK_LOG_DIR/conf

if [ "$SLURM_CPUS_PER_TASK" = "" ]; then
  SLURM_CPUS_PER_TASK=1
fi
CPU=$SLURM_CPUS_PER_TASK
if [ "$SLURM_MEM_PER_CPU" = "" ]; then
  MEM=$(( $SLURM_MEM_PER_NODE / $SLURM_CPUS_ON_NODE * $SLURM_CPUS_PER_TASK ))
else
  MEM=$(( $SLURM_MEM_PER_CPU * $SLURM_CPUS_PER_TASK ))
fi

#SPARK configs
if [ "$SPARK_HOME" = "" ]; then
  SPARK_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"
fi

if [ "$SPARK_MASTER_PORT" = "" ]; then
  SPARK_MASTER_PORT=7077
fi

if [ "$SPARK_HOST" = "" ]; then
  SPARK_HOST=`hostname -f`
fi

if [ "$SPARK_WEBUI_PORT" = "" ]; then
  SPARK_WEBUI_PORT=8080
fi

if [ "$SPARK_MASTER_WEBUI_PORT" = "" ]; then
  SPARK_MASTER_WEBUI_PORT=8080
fi

if [ "$SPARK_WORKER_WEBUI_PORT" = "" ]; then
  SPARK_WORKER_WEBUI_PORT=8081
fi

LOG=$SPARK_LOG_DIR/master.out
if [ "$SLURM_PROCID" = "0" ]; then
    $SPARK_HOME/bin/spark-class org.apache.spark.deploy.master.Master --host $SPARK_HOST --port $SPARK_MASTER_PORT --webui-port $SPARK_MASTER_WEBUI_PORT >> "$LOG" 2>&1 < /dev/null &

    while ! grep 'Starting Spark master at' $SPARK_LOG_DIR/*master*.out | grep -o 'spark://.*' &> /dev/null ; do
    sleep 1
    done
    MASTER=$(grep 'Starting Spark master at' $SPARK_LOG_DIR/*master*.out | grep -o 'spark://.*')
    echo -n "$MASTER" > $SPARK_LOG_DIR/master
    mkdir -p $SPARK_LOG_DIR/conf
    echo "spark.master     $MASTER" > $SPARK_LOG_DIR/conf/spark-defaults.conf

    while ! grep 'Bound MasterWebUI' $SPARK_LOG_DIR/*master*.out | grep -o 'http://.*' &> /dev/null ; do
    sleep 1
    done
    WEBUI=$(grep 'Bound MasterWebUI' $SPARK_LOG_DIR/*master*.out | grep -o 'http://.*')
    echo $WEBUI > $SPARK_LOG_DIR/master-console

    MEM=$(( $MEM>1024?$MEM - 1024 :0 ))

fi

while [ ! -s $SPARK_LOG_DIR/master ] ; do
  sleep 1
done

MASTER=$(cat $SPARK_LOG_DIR/master)
WORKER_NUM=$(( $SLURM_PROCID + 1 ))
PORT_NUM=$(( $SPARK_WORKER_PORT + $WORKER_NUM - 1 ))
WEBUI_PORT=$(( $SPARK_WORKER_WEBUI_PORT + $WORKER_NUM - 1 ))
WORKER_LOG=$SPARK_LOG_DIR/worker-${WORKER_NUM}.out

WORKER_WORKDIR=$SPARK_LOG_DIR/work

$SPARK_HOME/bin/spark-class org.apache.spark.deploy.worker.Worker --memory ${MEM}M --cores $CPU --work-dir $WORKER_WORKDIR \
  --webui-port $WEBUI_PORT "$MASTER" >> "$WORKER_LOG" 2>&1 < /dev/null &


tail -f /dev/null
